Q-metric
=========


Description
-----------

In the past few years, considerable attention has been paid to the development
of better video quality metrics that correlate well with the human perception
of quality. Although many metrics have been proposed, most of them are very
complex and require the original video for estimating the quality -- Full
Reference (FR) metrics. As a result, their use in real-time transmission
applications is very difficult. For these applications, the solution is to use
no-reference (NR) quality metrics, i.e., metrics that do not require the
original (reference) to estimate quality. In this project, we propose to
develop a library to develop no-reference metrics for estimating the strength
of video impairments. Considering this, Q-metrics is a project written in C++
using Qt and OpenCV. Its objective is provide a no-reference quality assessment
for videos.


License
-------

Q-metric is released under [GNU GPL version 2][10].


Requeriments
------------

To build this project you must have OpenCV 2.4.x and Qt4 installed on your
system.


Code access
-----------

1. Clone the source code
> `# hg clone https://bitbucket.org/kuraiev/q-metrics`
2. Compile using Makefile
> `# cd q-metrics`
> `# make`


Authorship and Copyright
------------------------

This code is developed by the [Digital Signal Processing Group][gpds] of 
[University of Brasília][UNB]. 


Contact
-------

Please send all comments, questions, reports and suggestions (especially if
you would like to contribute) to **sawp@sawp.com.br**.


References
----------

**Detection of blocking artifacts in compressed video**  
Vlachos, T. "Detection of blocking artifacts in compressed video." Electronics
Letters 36.13 (2000): 1106-1108.


**No-reference video quality metric based on artifact measurements**  
Farias, Mylene CQ, and Sanjit K. Mitra. "No-reference video quality metric
based on artifact measurements." Image Processing, 2005. ICIP 2005. IEEE
International Conference on. Vol. 3. IEEE, 2005.

**A perceptual distortion metric for digital color video**
Winkler, Stefan. "Perceptual distortion metric for digital color video."
Electronic Imaging'99. International Society for Optics and Photonics, 1999.


**What's wrong with mean-squared error?**  
Girod, Bernd. "What's wrong with mean-squared error?." Digital images and human
vision. MIT press, 1993.

**ITU Recommendation BT.500-8**  
Recommendation, I. T. U. R. B. T. "500-11, Methodology for the subjective
assessment of the quality of television pictures." International
Telecommunication Union, Geneva, Switzerland (2002).

**Structural Similarity Index (SSIM)**  
Z. Wang, A. C. Bovik, H. R. Sheikh, and E. P. Simoncelli, "Image quality 
assessment: From error visibility to structural similarity" IEEE Transactions 
on Image Processing, vol. 13, no. 4, pp.600-612, Apr. 2004

**Multi-scale SSIM Index (MSSIM)**  
Z. Wang, A. C. Bovik, H. R. Sheikh, and E. P. Simoncelli, "Image quality
assessment: From error visibility to structural similarity" IEEE Transactions
on Image Processing, vol. 13, no. 4, pp.600-612, Apr. 2004

**Noise Quality Measure (NQM)**  
N. Damera-Venkata, T. Kite, W. Geisler, B. Evans and A. Bovik, "Image Quality
Assessment Based on a Degradation Model", IEEE Trans. on Image Processing,
Vol. 9, No. 4, Apr. 2000

**Universal Image Quality Index (UQI)**  
Zhou Wang and Alan C. Bovik, "A Universal Image Quality Index", IEEE Signal
Processing Letters, 2001

**Visual Information Fidelity (VIF)**  
H. R. Sheikh and A. C. Bovik, "Image Information and Visual Quality"., IEEE
Transactions on Image Processing, (to appear).

**Weighted Signal-to-Noise Ratio (WSNR)**  
T. Mitsa and K. Varkur, "Evaluation of contrast sensitivity functions for the
formulation of quality measures incorporated in halftoning algorithms",
ICASSP '93-V, pp. 301-304.

**Signal-to-Noise Ratio (SNR, PSNR)**  
J. Mannos and D. Sakrison, "The effects of a visual fidelity criterion on the
encoding of images", IEEE Trans. Inf. Theory, IT-20(4), pp. 525-535, July 1974


Thanks
------

Special thanks to [Mylene C. Q. Farias][12] and 
[Hugo Tadashi Muniz Kussaba][11].


[gpds]: http://gpds.ene.unb.br/
[UNB]: http://www.unb.br
[10]: http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
[11]: http://lattes.cnpq.br/2586485528922581
[12]: http://www.pgea.unb.br/~mylene/
