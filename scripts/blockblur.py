#!/usr/bin/env python

import numpy as np
from scipy.misc import imshow
import cv2
import sys
import itertools as it

def normalize(arr):
    minval = arr.min()
    maxval = arr.max()
    return arr * (255.0 / (maxval-minval))

def blockingFrame(src, borderType = cv2.BORDER_DEFAULT):
    src_bordered = cv2.copyMakeBorder(src, 8, 8, 8, 8, borderType)
    (cols, rows, colors) = src.shape
    ret = np.zeros(src.shape)
    rows_in_blocks = int(rows / 8.0)
    cols_in_blocks = int(cols / 8.0)
    inner_block = np.zeros((8, 8, 3), np.uint8)
    outer_block = np.zeros((24, 24, 3), np.uint8)
    for i,j in it.product(xrange(rows_in_blocks), xrange(cols_in_blocks)):
        ROI = src[8*j:8*j+8, 8*i:8*i+8]
        inner_block[:,:] = src_bordered[8*j:8*j+8, 8*i:8*i+8]
        outer_block[:,:] = src_bordered[8*j:8*j+24, 8*i:8*i+24]
        ROI = ROI + 2.0 * abs(inner_block.mean() - outer_block.mean())
        ret[8*j:8*j+8, 8*i:8*i+8] = ROI[:,:,:]
    return ret

def blurringFrame(src, ksize=7):
    return cv2.medianBlur(src, ksize)

def blockAndBlur(frame, scale):
    frame_block = blockingFrame(frame).astype('float')
    frame_blur = blurringFrame(frame).astype('float')
    blended = cv2.addWeighted(frame_block, scale[0], frame_blur, scale[1], scale[2])
    normalized = normalize(blended)
    return normalized.astype('uint8')


def degradeFrame(frame, degradationType, scale):
    if degradationType == "blockandblur":
        return blockAndBlur(frame, scale)

def main(args):
    name = args[2]
    output = args[6]
    degradationType = args[7]
    scale = map(lambda x: float(x), (args[9], args[10], args[11]))
    cap = cv2.VideoCapture(name)
    fourcc = cv2.cv.CV_FOURCC('I', '4', '2', '0')
    fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
    height = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
    width =int( cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
    out = cv2.VideoWriter(output, fourcc, fps, (width, height))
    while(cap.isOpened()):
        ret, frame = cap.read()
        if ret == True:
            processed = degradeFrame(frame, degradationType, scale)
            out.write(processed)
        else:
          break
    cap.release()
    out.release()

if __name__ == "__main__":
    main(sys.argv)

